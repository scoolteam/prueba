# vue-semilla

> semilla de vue

## Build Setup

``` bash
# install dependencies
npm install

Copiar /build/properties.default.js en /build/properties.js y cambiar rutas de producción a 
desarrollo:
{
    ...
    PUBLICPATH: '/',
    CONTEXT: JSON.stringify('/')
}

# serve with hot reload at localhost:8090
npm run dev

# build for production with minification
npm run build



