import Vue from "vue";
import Router from "vue-router";
import Inicio from "@/pages/Inicio";

Vue.use(Router);

const router = new Router({
  base: CONTEXT,
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Inicio",
      component: Inicio
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

export default router;
