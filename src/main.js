import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import App from "./App";
import router from "./router";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { ObserveVisibility } from 'vue-observe-visibility'
require('@/assets/css/style.css')


Vue.use(BootstrapVue);
Vue.config.productionTip = false;
Vue.directive('observe-visibility', ObserveVisibility)

new Vue({
  el: "#app",
  router,
  template: "<App/>",
  components: { App }
});
